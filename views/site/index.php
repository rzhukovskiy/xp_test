<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\CompanyForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

$this->title = 'Check company symbol';
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to check company history:</p>

    <?php $form = ActiveForm::begin([
        'action' => 'site/result',
        'id' => 'company-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'companySymbol', [
        'enableAjaxValidation' => true,
    ])->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'email')->textInput() ?>

    <?= $form->field($model, 'startDate', [
        'enableAjaxValidation' => true,
    ])->widget(DatePicker::class, [
        'options' => ['placeholder' => 'Enter start date'],
        'removeButton' => false,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]); ?>

    <?= $form->field($model, 'endDate', [
        'enableAjaxValidation' => true,
    ])->widget(DatePicker::class, [
        'options' => ['placeholder' => 'Enter end date'],
        'removeButton' => false,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]); ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Check', ['class' => 'btn btn-primary', 'name' => 'check-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
