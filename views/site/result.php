<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $headers array */

/* @var $dataProvider \yii\data\ArrayDataProvider */

use yii\grid\GridView;
use \sjaakp\gcharts\LineChart;

$this->title = 'Result';
?>
<?= \yii\helpers\Html::a('Back', '/')?>

<?= LineChart::widget([
    'height' => '400px',
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'Date',
            'type' => 'date'
        ],
        'Open',
        'Close',
    ],
    'options' => [
        'title' => 'Open and close prices'
    ],
]);
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $headers,
]); ?>
