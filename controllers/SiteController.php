<?php

namespace app\controllers;

use app\services\CsvService;
use app\services\NasdaqService;
use app\services\QuandlService;
use app\models\CompanyForm;
use Yii;
use yii\base\Module;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
    /** @var $quandlService QuandlService */
    private $quandlService;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function __construct(
        string $id,
        Module $module,
        QuandlService $quandlService,
        array $config = []
    ) {
        $this->quandlService = $quandlService;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return array|string|Response
     */
    public function actionIndex()
    {
        $model = new CompanyForm();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * @return array|string
     */
    public function actionResult()
    {
        $model = new CompanyForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($model->email)
                ->setFrom('GreyShtall@yandex.ru')
                ->setSubject($model->companySymbol)
                ->setTextBody($model->startDate . ' - ' . $model->endDate)
                ->send();

            $csvService = new CsvService($this->quandlService->buildHistoryUrl($model));
            $historyData = [];
            foreach ($csvService->parse() as $row) {
                $historyData[] = $row;
            }
            $dataProvider = new ArrayDataProvider([
                'allModels' => $historyData,
                'pagination' => false,
            ]);

            return $this->render('result', [
                'headers' => $csvService->getHeaders(),
                'dataProvider' => $dataProvider,
            ]);
        }
    }
}
