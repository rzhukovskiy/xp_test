<?php

namespace app\models;

use app\services\CsvService;
use yii\base\Model;

/**
 * Class CompanyForm
 * @package app\models
 */
class CompanyForm extends Model
{
    public $companySymbol;
    public $email;
    public $startDate;
    public $endDate;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['companySymbol', 'email', 'startDate', 'endDate'], 'required'],
            ['companySymbol', 'string'],
            ['companySymbol', 'validateSymbol'],
            [['startDate', 'endDate'], 'validateDate'],
            [['startDate', 'endDate'], 'date', 'format' => 'php:Y-m-d'],
            ['startDate', 'default', 'value' => date('Y-m-d')],
            ['endDate', 'default', 'value' => date('Y-m-d')],
            ['email', 'email'],
        ];
    }

    public function validateDate($attribute, $params)
    {
        $startDate = \DateTime::createFromFormat ( 'Y-m-d' , $this->startDate);
        $endDate = \DateTime::createFromFormat ( 'Y-m-d' , $this->endDate);

        if ($startDate > $endDate) {
            $this->addError($attribute, 'Incorrect data. Start date should be less than end date');
        }
    }

    public function validateSymbol($attribute, $params)
    {
        $csvService = new CsvService('static/companylist.csv');
        foreach ($csvService->parse() as $companyData) {
            if ($companyData['Symbol'] == $this->companySymbol) {
                return;
            }
        }

        $this->addError($attribute, 'Company symbol doesn`t exist.');
    }
}
