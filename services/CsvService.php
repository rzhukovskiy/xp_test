<?php
/**
 * Created by PhpStorm.
 * User: shtal
 * Date: 23.12.2018
 * Time: 15:07
 */

namespace app\services;


/**
 * Class CsvService
 * @package app\services
 */
class CsvService
{
    private $file;
    private $headers;

    /**
     * CsvService constructor.
     * @param $file
     */
    public function __construct($file) {
        $this->file = fopen($file, 'r');
    }

    /**
     * @return array
     */
    public function getHeaders() {
        return $this->headers;
    }

    /**
     * @return \Generator|void
     */
    public function parse() {
        $headers = array_map('trim', fgetcsv($this->file, 4096));
        $this->headers = $headers;
        while (!feof($this->file)) {
            $row = array_map('trim', (array)fgetcsv($this->file, 4096));
            if (count($headers) !== count($row)) {
                continue;
            }
            $row = array_combine($headers, $row);
            yield $row;
        }
        return;
    }
}