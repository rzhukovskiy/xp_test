<?php

namespace app\services;

use app\models\CompanyForm;
use yii\base\BaseObject;
use yii\httpclient\Client;

/**
 * Created by PhpStorm.
 * User: shtal
 * Date: 22.12.2018
 * Time: 19:52
 */

/**
 * Class QuandlService
 * @package app\services
 */
class QuandlService extends BaseObject
{
    /** @var $httpClient Client */
    private $httpClient;
    /** @var $url string */
    public $url;

    /**
     * QuandlService constructor.
     * @param Client $httpClient
     * @param array $config
     */
    public function __construct(Client $httpClient, array $config = [])
    {
        $this->httpClient = $httpClient;
        parent::__construct($config);
    }

    /**
     * @param CompanyForm $companyFrom
     * @return string
     */
    public function buildHistoryUrl(CompanyForm $companyFrom)
    {
        return $this->url
            . $companyFrom->companySymbol
            . '.csv?order=asc&start_date=' . $companyFrom->startDate
            . '&end_date=' . $companyFrom->endDate;
    }
}